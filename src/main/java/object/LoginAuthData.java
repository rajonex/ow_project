package object;

public class LoginAuthData {
    private boolean success;
    private String token;

    public LoginAuthData(boolean success, String token) {
        this.success = success;
        this.token = token;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getToken() {
        return token;
    }
}
