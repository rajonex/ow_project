package object;

public class ConfigData {
    private String address;
    private int port;
    private String username;
    private String password;
    private String engine;

    public ConfigData(String address, int port, String username, String password, String engine) {
        this.address = address;
        this.port = port;
        this.username = username;
        this.password = password;
        this.engine = engine;
    }

    public String getAddress() {
        return address;
    }

    public int getPort() {
        return port;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getEngine() {
        return engine;
    }

    @Override
    public String toString() {
        return "ConfigData{" +
                "address='" + address + '\'' +
                ", port=" + port +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", engine='" + engine + '\'' +
                '}';
    }
}
