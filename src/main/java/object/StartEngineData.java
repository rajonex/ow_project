package object;

public class StartEngineData {
    private String engine;

    public String getEngine() {
        return engine;
    }

    public StartEngineData(String engine) {
        this.engine = engine;
    }
}
