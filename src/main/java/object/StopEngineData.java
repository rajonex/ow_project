package object;

public class StopEngineData {
    private boolean success;
    private String info;

    public StopEngineData(boolean success, String info) {
        this.success = success;
        this.info = info;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getInfo() {
        return info;
    }
}
