package websocket;

import okhttp3.*;

import java.io.IOException;

public class HttpConnect {

    private OkHttpClient client;

    public HttpConnect() {
        this.client = new OkHttpClient();
    }

    public String basePost(String url, String json){
        RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), json);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();

        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        } catch (IOException e){
            e.printStackTrace();
        }

        return null;
    }

    public String authPost(String url, String json, String token){
        RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), json);
        Request request = new Request.Builder()
                .url(url)
                .addHeader("Authorization", "Bearer " + token)
                .post(body)
                .build();

        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        } catch (IOException e){
            e.printStackTrace();
        }

        return null;
    }

    public int authGet(String url, String token){
        Request request = new Request.Builder()
                .url(url)
                .addHeader("Authorization", "Bearer " + token)
                .get()
                .build();

        try (Response response = client.newCall(request).execute()) {
            return response.code();
        } catch (IOException e){
            e.printStackTrace();
        }
        return -1;
    }

    public String authPostWithoutBody(String url, String token){
        RequestBody body = RequestBody.create(null, new byte[0]);
        Request request = new Request.Builder()
                .url(url)
                .addHeader("Authorization", "Bearer " + token)
                .post(body)
                .build();

        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        } catch (IOException e){
            e.printStackTrace();
        }
        return null;
    }
}
