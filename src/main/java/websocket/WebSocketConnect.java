package websocket;


import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import sun.rmi.runtime.Log;

import java.io.IOException;

public class WebSocketConnect{
    private WebSocket webSocket;
    private boolean connected;
    private String token;


    public WebSocketConnect(String token) {
        this.token = token;
    }

    public void connectToWebSocket(String url) {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .addHeader("Authorization", "Bearer " + token)
                .url(url)
                .build();
        webSocket = client.newWebSocket(request, new WebSocketListener());
        client.dispatcher().executorService().shutdown();
        client.connectionPool().evictAll();
    }

    public void sendMessageThroughWebSocket(String message) {
        if (!connected) {
            return;
        }
        if (!webSocket.send(message)) {
            System.out.println("Failed to send message");
        } else {
//            System.out.println(message);
        }
    }

    public void closeWebSocket() {
        if (!connected) {
            return;
        }
        if (!webSocket.close(1000, "Socket closed.")) {
            System.out.println("Failed to close WebSocket");
        }
//        System.out.println("Session closed");
    }



    private final class WebSocketListener extends okhttp3.WebSocketListener {
        @Override
        public void onOpen(WebSocket webSocket, Response response) {
            super.onOpen(webSocket, response);
            connected = true;
//            System.out.println("Web socket opened");
        }

        @Override
        public void onMessage(WebSocket webSocket, String text) {
            super.onMessage(webSocket, text);
            System.out.println(text);
        }

        @Override
        public void onClosed(WebSocket webSocket, int code, String reason) {
            connected = false;
//            System.out.println("Web socket closed reason: " + reason);
        }

        @Override
        public void onFailure(WebSocket webSocket, Throwable t, Response response) {
            connected = false;
//            System.out.println("Web socket failed ");
        }
    }
}









