package app;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import object.ConfigData;
import object.LoginAuthData;
import object.LoginData;
import object.StartEngineData;
import websocket.HttpConnect;
import websocket.WebSocketConnect;

import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class Main {


    public static void main(String... args) throws IOException {
//        System.out.println("Started");
        Gson gson = new Gson();
        HttpConnect httpConnect = new HttpConnect();
        JsonReader reader = new JsonReader(new FileReader("config.json"));
        ConfigData configData = gson.fromJson(reader, ConfigData.class);

        String httpAddress = "http://" + configData.getAddress() + ":" + configData.getPort();
        String wsAddress = "ws://" + configData.getAddress() + ":" + configData.getPort();

//
        LoginData loginData = new LoginData(configData.getUsername(), configData.getPassword());
        String tokenString = httpConnect.basePost( httpAddress + "/user/login", gson.toJson(loginData ,LoginData.class));
        String token = gson.fromJson(tokenString, LoginAuthData.class).getToken();
        httpConnect.authPost(httpAddress + "/engine/start", gson.toJson(new StartEngineData(configData.getEngine())), token);

        WebSocketConnect wsc = new WebSocketConnect(token);
        wsc.connectToWebSocket( wsAddress + "/ws_engine");
        Runtime.getRuntime().addShutdownHook(new Thread()
        {
            @Override
            public void run()
            {
//                System.out.println("Wychodze");
                httpConnect.authPostWithoutBody(httpAddress + "/engine/stop", token);
                httpConnect.authGet(httpAddress + "/user/logout", token);
//                wsc.closeWebSocket();

            }
        });
        Scanner input = new Scanner(System.in);
        String text = input.nextLine();
        while(!text.equals("exit")){
            wsc.sendMessageThroughWebSocket(text);
            text = input.nextLine();
        }
        wsc.closeWebSocket();
        httpConnect.authPostWithoutBody(httpAddress + "/engine/stop", token);
        httpConnect.authGet(httpAddress + "/user/logout", token);
    }
}
